Game consists of four View Controllers:

1) InitialViewController : Entry point of application.

![intro.png](https://bitbucket.org/repo/z8jjLoo/images/504831218-intro.png)

2) PrepareModeViewController : players can entry their names and start the
 game.

![prepare.png](https://bitbucket.org/repo/z8jjLoo/images/348300794-prepare.png)

3) LocateModeViewController : players arrange their ships in turns. Tap on any ship to change it rotation. “Drag & Drop ” the ship to move it to the field. Button “Next player/Battle!” becomes active when player locates all his/her ships. You can’t locate ships next to each other. Overall, the player should locate 4 single, 3 double, 2 triple and 1 quadruple ship.

![locate1.png](https://bitbucket.org/repo/z8jjLoo/images/3223582241-locate1.png)

4) BattleModeViewController : players go by turns. First player starts the game. The aim of “BattleShip” is to destroy your opponent’s naval army. “Back” button will return you to InitialViewController.

![battle.png](https://bitbucket.org/repo/z8jjLoo/images/3649471585-battle.png)

Also, rules you can find here: https://en.wikipedia.org/wiki/Battleship_(puzzle)