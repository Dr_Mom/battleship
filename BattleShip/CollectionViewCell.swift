//
//  CollectionViewCell.swift
//  BattleShip
//
//  Created by Andrei Momot on 3/7/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
  enum CollectionIdentifier: String {
    case cell = "CollectionViewCell"
  }

  override init(frame: CGRect) {
    super.init(frame: frame)

    layer.borderWidth = 0.5
    layer.borderColor = UIColor.blue.cgColor
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)

    layer.borderWidth = 0.5
    layer.borderColor = UIColor.blue.cgColor
  }

  class func cellReuseIdentifier() -> String! {
    return CollectionIdentifier.cell.rawValue
  }

  class func cellSize() -> CGFloat {
    return 25.0
  }
}
