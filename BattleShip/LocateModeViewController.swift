//
//  LocateModeViewController.swift
//  BattleShip
//
//  Created by Andrei Momot on 3/7/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import UIKit

let shipLeftMargin: CGFloat = 30
let shipTopMargin: CGFloat = 35

enum ShipSize: Int {
  case Submarine = 1
  case Destroyer = 2
  case Cruiser = 3
  case Battleship = 4

  func origin() -> CGPoint {
    switch self {
    case .Battleship:
      return CGPoint(x: shipLeftMargin, y: shipTopMargin + 9*CollectionViewCell.cellSize())
    case .Cruiser:
      return CGPoint(x: shipLeftMargin, y: shipTopMargin + 5*CollectionViewCell.cellSize())
    case .Destroyer:
      return CGPoint(x: shipLeftMargin, y: shipTopMargin + 2*CollectionViewCell.cellSize())
    case .Submarine:
      return CGPoint(x: shipLeftMargin, y: shipTopMargin)
    }
  }
}

class LocateModeViewController: UIViewController, ShipViewDelegate {
  enum SegueIdentifier: String {
    case locateModeSecond = "SegueToNextLocateModeViewController"
    case battle = "SegueToBattleModeViewController"
  }

  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var fieldCollectionView: CollectionView!
  @IBOutlet weak var nextButton: UIButton!

  var currentPlayerNumber: PlayerNumber?

    override func viewDidLoad() {
      super.viewDidLoad()
      nextButton.configureCustomButton()
      Battle.attachCollectionViewToPlayerWithNumber(number: self.currentPlayerNumber!,
                                                    collectionView: self.fieldCollectionView,
                                                    battleMode: false)
      self.prepareShips()

      self.titleLabel.text = Battle.titleForLocate(number: self.currentPlayerNumber)
      self.nextButton.setTitle(Battle.nextButtonLocateTitle(number: self.currentPlayerNumber), for: .normal)
      self.checkNextButtonAvailability()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.fieldCollectionView.reloadItems(at: self.fieldCollectionView.indexPathsForVisibleItems)
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    guard segue.identifier != nil else {
      return
    }

    switch segue.identifier! {
    case SegueIdentifier.locateModeSecond.rawValue:
      if let locateModeViewController = segue.destination as? LocateModeViewController {
        locateModeViewController.currentPlayerNumber = .Second
      }

      break
    case SegueIdentifier.battle.rawValue:
      break
    default:
      break
    }
  }

  private func checkNextButtonAvailability() {
    self.nextButton.isEnabled = Battle.playerWithNumber(number: self.currentPlayerNumber!).amountOfFreeShips == 0
    if nextButton.isEnabled {
      self.nextButton.setTitleColor(.white, for: .normal)
    }
  }

  private func prepareShips() {
    for i in 0..<4 {
      let size = 4 - i
      let count = i + 1

      for _ in 0..<count {
        let shipView = ShipView(size: size, defaultPoint: ShipSize(rawValue: size)?.origin() ?? CGPoint.zero)
        shipView.delegate = self

        self.view.addSubview(shipView)
      }
    }
  }

  // MARK: Ships View Delegate
  func itemDidTapped(view: ShipView) {
  }

  func itemDidDragged(view: ShipView) {
    let coordinates = self.fieldCollectionView.coordinatesFromShipFrame(frame: view.frame)
    if coordinates.count == view.size && Battle.playerWithNumber(number: self.currentPlayerNumber!)
      .field.canAddShip(coordinatesArray: coordinates) {
      view.layer.borderColor = UIColor.green.cgColor
    } else {
      view.layer.borderColor = UIColor.red.cgColor
    }
  }

  func itemDidDropped(view: ShipView) {
    let coordinates = self.fieldCollectionView.coordinatesFromShipFrame(frame: view.frame)
    if coordinates.count == view.size && Battle.playerWithNumber(number: self.currentPlayerNumber!)
      .field.canAddShip(coordinatesArray: coordinates) {
      let ship = Ship(coordinates: coordinates)

      Battle.playerWithNumber(number: self.currentPlayerNumber!).field.addShip(ship: ship)
      view.removeFromSuperview()

      self.fieldCollectionView.reloadItems(at: self.fieldCollectionView.indexPathsForVisibleItems)
      self.checkNextButtonAvailability()
    } else {
      view.resetLocation()
    }
  }
}
