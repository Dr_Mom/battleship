//
//  PrepareModeViewController.swift
//  BattleShip
//
//  Created by Andrei Momot on 3/7/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import UIKit

class PrepareModeViewController: UIViewController {
  enum SegueIdentifier: String {
    case locate = "SegueToLocateModeViewController"
  }

  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var firstPlayerLabel: UILabel!
  @IBOutlet weak var secondPlayerLabel: UILabel!
  @IBOutlet weak var firstPlayerTextField: UITextField!
  @IBOutlet weak var secondPlayerTextField: UITextField!
  @IBOutlet weak var startButton: UIButton!

  override func viewDidLoad() {
    super.viewDidLoad()
    startButton.configureCustomButton()
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == SegueIdentifier.locate.rawValue {
      Battle.sharedBattle().initialize(firstPlayerName: firstPlayerTextField.text,
                                       secondPlayerName: secondPlayerTextField.text)

      if let locateModeViewController = segue.destination as? LocateModeViewController {
        locateModeViewController.currentPlayerNumber = .First
      }
    }
  }
}
