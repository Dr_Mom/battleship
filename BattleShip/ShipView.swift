//
//  ShipView.swift
//  BattleShip
//
//  Created by Andrei Momot on 3/7/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import UIKit

protocol ShipViewDelegate: class {
  func itemDidDragged(view: ShipView)
  func itemDidDropped(view: ShipView)
  func itemDidTapped(view: ShipView)
}

class ShipView: UIView {
  enum KeyIdentifier: String {
    case cellSize
  }

  private var cellSize: CGFloat
  private var isMoved: Bool
  private var defaultPoint: CGPoint

  weak var delegate: ShipViewDelegate?

  var size: Int {
      return Int(self.frame.width > self.frame.height ? self.frame.width/self.cellSize: self.frame.height/self.cellSize)
  }

  init(size: Int, defaultPoint: CGPoint, cellSize: CGFloat = CollectionViewCell.cellSize()) {
    self.cellSize = cellSize
    self.isMoved = false
    self.defaultPoint = defaultPoint

    super.init(frame: CGRect(x: defaultPoint.x, y: defaultPoint.y, width: cellSize * CGFloat(size), height: cellSize))

    self.backgroundColor = CellState.Filled.color(battleMode: false)
    self.layer.borderWidth = 1
    self.layer.borderColor = UIColor.clear.cgColor
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    self.defaultPoint = self.frame.origin
  }

  required init?(coder aDecoder: NSCoder) {
    self.isMoved = false
    self.cellSize = aDecoder.decodeObject(forKey: KeyIdentifier.cellSize.rawValue)
                                          as? CGFloat ?? CollectionViewCell.cellSize()
    self.defaultPoint = CGPoint(x: 0, y: 0)

    super.init(coder: aDecoder)
  }

  private func rotate() {
    let difference = self.frame.width - self.frame.height
    self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y - difference,
                        width: self.frame.height, height: self.frame.width)
    self.defaultPoint = self.frame.origin
    self.setNeedsDisplay()
  }

  func resetLocation() {
    self.frame.origin = self.defaultPoint
    self.layer.borderColor = UIColor.clear.cgColor
    self.setNeedsDisplay()
  }

  // MARK: UITouch Methods
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.alpha = 0.25
    self.delegate?.itemDidDragged(view: self)
  }

  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.isMoved = true
    let touch = touches.first!
    let touchPoint = touch.location(in: self)
    let previousPoint = touch.previousLocation(in: self)

    self.center = CGPoint(x: self.center.x + touchPoint.x - previousPoint.x,
                          y: self.center.y + touchPoint.y - previousPoint.y)
    self.delegate?.itemDidDragged(view: self)
  }

  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.alpha = 1.0
    if self.isMoved {
      self.delegate?.itemDidDropped(view: self)
      self.isMoved = false
    } else {
      self.rotate()
      self.delegate?.itemDidTapped(view: self)
    }
  }

  override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.touchesEnded(touches, with: event)
  }
}
