//
//  Player.swift
//  BattleShip
//
//  Created by Andrei Momot on 3/9/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import Foundation

class Player: NSObject {
  var name: String
  var field = BattleField()
  weak var battle: Battle?

  var isShipsAvailable: Bool {
      return self.field.aliveShips.count > 0
  }

  var coordinatesDictionary: [Coordinate: CellState] {
      return self.field.coordinatesDictionary
  }

  var amountOfFreeShips: Int {
      return 10 - self.field.ships.count
  }

  init(name: String, battle: Battle? = nil) {
    self.name = name
    self.battle = battle
    super.init()
  }

  func setName(name: String) {
    guard !name.isEmpty else {
      return
    }
    self.name = name
  }

  func proceedHit(coordinate: Coordinate) {
    let success = self.field.proceedHit(coordinate: coordinate)
    guard success else {
      return
    }
  }

  func reloadShips() {
    self.field.reloadField()
  }
}
