//
//  Ship.swift
//  BattleShip
//
//  Created by Andrei Momot on 3/9/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import Foundation

enum ShipState {
  case Unbroken
  case Damaged
  case Destroyed
}

class Ship: NSObject {
  private var coordinates: [Coordinate]
  private var state: ShipState = .Unbroken

  weak var field: BattleField?
  var coordinatesArray: [Coordinate] {
      return self.coordinates
  }

  var shipState: ShipState {
      return self.state
  }

  var undamagedCoordinates: [Coordinate] {
      var coordinates = [Coordinate]()
      for coordinate in self.coordinates {
        if self.field?.coordinatesDictionary[coordinate] == .Filled {
          coordinates.append(coordinate)
      }
    }
    return coordinates
  }

  var size: Int

  init(coordinates: [Coordinate], field: BattleField? = nil) {
    self.coordinates = coordinates
    self.size = coordinates.count
    self.field = field
    super.init()
  }

  init(size: Int, field: BattleField? = nil) {
    self.coordinates = [Coordinate]()
    self.size = size
    self.field = field
    super.init()
  }

  func rotate() {
    guard self.coordinates.count > 1 else {
      return
    }

    for i in 1..<self.coordinates.count {
      var coordinate = self.coordinates[i]
      coordinate.inverse()
      if self.field?.coordinatesDictionary[coordinate] != .Empty {
        return
      }
    }

    for i in 1..<self.coordinates.count {
      self.coordinates[i].inverse()
    }
  }

  func isContainsCoordinate(coordinate: Coordinate) -> Bool {
    return self.coordinates.contains(coordinate)
  }

  // If hit = true, otherwise = false
  func proceedHit(coordinate: Coordinate) -> Bool {
    guard self.coordinates.contains(coordinate) else {
      return false
    }

    let unbrokenCoordinates = self.undamagedCoordinates
    state = unbrokenCoordinates.count == 1 && unbrokenCoordinates.first! == coordinate ? .Destroyed : .Damaged
    return true
  }
}
