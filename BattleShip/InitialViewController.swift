//
//  InitialViewController.swift
//  BattleShip
//
//  Created by Andrei Momot on 3/7/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {
  @IBOutlet weak var newGameButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        newGameButton.configureCustomButton()
    }

  @IBAction func unwindHome(segue: UIStoryboardSegue) {
  }
}
