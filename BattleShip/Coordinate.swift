//
//  Coordinate.swift
//  BattleShip
//
//  Created by Andrei Momot on 3/7/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import UIKit

struct Coordinate: Hashable {
  var vertical, horizontal: Int

  init(vertical: Int, horizontal: Int) {
    self.vertical = vertical
    self.horizontal = horizontal
  }

  init(indexPath: IndexPath) {
    let horizontal = indexPath.row % 10
    let vertical = (indexPath.row - horizontal) / 10

    self.init(vertical: vertical, horizontal: horizontal)
  }

  mutating func inverse() {
    let temp = self.horizontal
    self.horizontal = self.vertical
    self.vertical = temp
  }

  // MARK: Hashable
  var hashValue: Int {

    return vertical.hashValue ^ horizontal.hashValue
  }

  static func == (left: Coordinate, right: Coordinate) -> Bool {
    return (left.vertical == right.vertical) && (left.horizontal == right.horizontal)
  }
}
