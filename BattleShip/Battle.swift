//
//  Battle.swift
//  BattleShip
//
//  Created by Andrei Momot on 3/7/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import Foundation

enum PlayerNumber: Int {
  case First = 0
  case Second = 1
}

struct GameNotification {
  static let FirstPlayerWon = NSNotification.Name(rawValue:"first_player_won")
  static let SecondPlayerWon = NSNotification.Name(rawValue:"second_player_won")
  static let GameFinished = NSNotification.Name(rawValue:"game_finished")
  static let PlayerSwitched = NSNotification.Name(rawValue: "player_switched")
}

class Battle: NSObject {
  enum StringIdentifier: String {
    case first = "Player1"
    case second = "Player2"
    case error = "Wrong player!"
    case locateTitle = ", let`s build your naval army!"
    case firstButtonTitle = "Next player"
    case secondButtonTitle = "Battle!"
  }

  private static var battle = Battle()

  var firstPlayer, secondPlayer: Player
  var currentPlayerNumber = PlayerNumber.First

  static var currentPlayer: Player {
      return Battle.playerWithNumber(number: Battle.sharedBattle().currentPlayerNumber)
  }

  func initialize(firstPlayerName: String?, secondPlayerName: String?) {
    self.firstPlayer.setName(name: firstPlayerName ?? StringIdentifier.first.rawValue)
    self.secondPlayer.setName(name: secondPlayerName ?? StringIdentifier.second.rawValue)
  }

  private override init() {
    self.firstPlayer = Player(name: StringIdentifier.first.rawValue)
    self.secondPlayer = Player(name: StringIdentifier.second.rawValue)

    super.init()

    self.firstPlayer.battle = self
    self.secondPlayer.battle = self
  }

  static func sharedBattle() -> Battle {
    return battle
  }

  static func playerWithNumber(number: PlayerNumber) -> Player {
    switch number {
    case .First:
      return Battle.sharedBattle().firstPlayer
    case .Second:
      return Battle.sharedBattle().secondPlayer
    }
  }

  static func titleForLocate(number: PlayerNumber?) -> String {
    guard number != nil else {
      return String(StringIdentifier.error.rawValue)
    }

    switch number! {
    case .First:
      return String("\(Battle.sharedBattle().firstPlayer.name) \(StringIdentifier.locateTitle.rawValue)")
    case .Second:
      return String("\(Battle.sharedBattle().secondPlayer.name)\(StringIdentifier.locateTitle.rawValue)")
    }
  }

  static func nextButtonLocateTitle(number: PlayerNumber?) -> String {
    guard number != nil else {
      return String(StringIdentifier.error.rawValue)
    }

    switch number! {
    case .First:
      return StringIdentifier.firstButtonTitle.rawValue
    case .Second:
      return StringIdentifier.secondButtonTitle.rawValue
    }
  }

  static func attachCollectionViewToPlayerWithNumber(number: PlayerNumber,
                                                     collectionView: CollectionView,
                                                     battleMode: Bool = true) {
    let field = self.playerWithNumber(number: number).field
    field.isBattleMode = battleMode
    collectionView.delegate = field
    collectionView.dataSource = field
  }

  static func getWinnerName() -> Player? {
    let playersArray = [Battle.sharedBattle().firstPlayer, Battle.sharedBattle().secondPlayer]
    for player in playersArray {
      if player.field.aliveShips.count > 0 {
        return player
      }
    }

    return nil
  }

  static func reloadGame() {
    Battle.sharedBattle().firstPlayer.reloadShips()
    Battle.sharedBattle().secondPlayer.reloadShips()
    Battle.sharedBattle().currentPlayerNumber = .First
    Battle.sharedBattle().firstPlayer.setName(name: StringIdentifier.first.rawValue)
    Battle.sharedBattle().secondPlayer.setName(name: StringIdentifier.second.rawValue)
  }

  static func switchPlayer() {
    Battle.sharedBattle().currentPlayerNumber = Battle.sharedBattle().currentPlayerNumber == .First ? .Second : .First
    NotificationCenter.default.post(name: GameNotification.PlayerSwitched, object: nil)
  }
}
