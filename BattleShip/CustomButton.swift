//
//  CustomButton.swift
//  BattleShip
//
//  Created by Andrei Momot on 3/9/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import UIKit

extension UIButton {
  func configureCustomButton() {
    self.layer.cornerRadius = 3
    self.layer.borderWidth = 1
    self.layer.borderColor = UIColor.white.cgColor
  }
}
