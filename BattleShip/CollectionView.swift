//
//  CollectionView.swift
//  BattleShip
//
//  Created by Andrei Momot on 3/7/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import UIKit

class CollectionView: UICollectionView {
  override func awakeFromNib() {
    super.awakeFromNib()
    self.layer.borderWidth = 2.0
    self.layer.borderColor = UIColor.black.cgColor
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  override func cellForItem(at indexPath: IndexPath) -> UICollectionViewCell? {
    let cell = super.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.cellReuseIdentifier(), for: indexPath)
    return cell
  }

  func coordinatesFromShipFrame(frame: CGRect) -> [Coordinate] {
    var coordinatesArray = [Coordinate]()

    let pointInCollectionView = CGPoint(x: frame.origin.x - self.frame.origin.x,
                                        y: frame.origin.y - self.frame.origin.y)
    guard  pointInCollectionView.x > 0 &&  pointInCollectionView.y > 0 else {
      return coordinatesArray
    }

    let orientation = frame.width >= frame.height
    let length = frame.width >= frame.height ? frame.width : frame.height
    let cellSize = CollectionViewCell.cellSize()
    let size = Int(length/cellSize)

    let startPoint = CGPoint(x: pointInCollectionView.x + cellSize/2, y: pointInCollectionView.y + cellSize/2)

    for i in 0..<size {
      var point: CGPoint
      if orientation {
        point = CGPoint(x: startPoint.x + CGFloat(i)*cellSize, y: startPoint.y)
      } else {
        point = CGPoint(x: startPoint.x, y: startPoint.y + CGFloat(i)*cellSize)
      }

      if let indexPath = self.indexPathForItem(at: point) {
        let coordinate =  Coordinate(indexPath: indexPath)
        coordinatesArray.append(coordinate)
      }
    }

    return coordinatesArray
  }
}
