//
//  BattleField.swift
//  BattleShip
//
//  Created by Andrei Momot on 3/8/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import UIKit

enum CellState {
  case Empty
  case Filled
  case Missed
  case Damaged
  case Destroyed

  func color(battleMode: Bool = true) -> UIColor {
    switch self {
    case .Empty:
      return UIColor.white
    case .Filled:
      return battleMode ? UIColor.white : UIColor.blue
    case .Missed:
      return UIColor.black
    case .Damaged:
      return UIColor.orange
    case .Destroyed:
      return UIColor.red
    }
  }
}

class BattleField: NSObject {
  var isBattleMode = false
  var coordinatesDictionary = [Coordinate: CellState]()
  var ships = [Ship]()

  var aliveShips: [Ship] {
      var shipsArray = [Ship]()
      for ship in self.ships {
        if ship.shipState != .Destroyed {
          shipsArray.append(ship)
      }
    }
    return shipsArray
  }

  override init() {
    super.init()

    for horizontalIdentifier in 0..<10 {
      for verticalIdentifier in 0..<10 {
        self.coordinatesDictionary[Coordinate(vertical: verticalIdentifier, horizontal: horizontalIdentifier)] = .Empty
      }
    }
  }

  private func neighborCoordinates(coordinate: Coordinate, completion: (Coordinate) -> Void) {
    let horizontalLowerBound = max(0, coordinate.horizontal - 1)
    let horizontalUpperBound = min(coordinate.horizontal + 2, 10)
    let verticalLowerBound = max(0, coordinate.vertical - 1)
    let verticalUpperBound = min(coordinate.vertical + 2, 10)

    for horizontalIdentifier in horizontalLowerBound..<horizontalUpperBound {
      for verticalIdentifier in verticalLowerBound..<verticalUpperBound {
        completion(Coordinate(vertical: verticalIdentifier, horizontal: horizontalIdentifier))
      }
    }
  }

  func canAddShip(coordinatesArray: [Coordinate]) -> Bool {
    guard self.ships.count < 10 && coordinatesArray.count > 0 else {
      return false
    }

    for coordinate in coordinatesArray {
      guard self.isCoordinateAvailable(coordinate: coordinate) else {
        return false
      }
    }
    return true
  }

  func addShip(ship: Ship) {
    guard self.canAddShip(coordinatesArray: ship.coordinatesArray) else {
      return
    }

    ship.field = self
    self.ships.append(ship)
    for coordinate in ship.coordinatesArray {
      self.coordinatesDictionary[coordinate] = .Filled
    }
  }

  func isCoordinateAvailable(coordinate: Coordinate) -> Bool {
    guard self.coordinatesDictionary[coordinate] == .Empty else {
      return false
    }

    var available = true
    self.neighborCoordinates(coordinate: coordinate, completion: {coordinate in
      if self.coordinatesDictionary[coordinate] != .Empty {
        available = false
        return
      }
    })

    return available
  }

  func getShipWithCoordinate(coordinate: Coordinate) -> Ship? {
    for ship in self.aliveShips {
      if ship.isContainsCoordinate(coordinate: coordinate) {
        return ship
      }
    }

    return nil
  }

  // If hit = true, otherwise = false
  func proceedHit(coordinate: Coordinate) -> Bool {
    guard let cellState = self.coordinatesDictionary[coordinate] else {
      self.coordinatesDictionary[coordinate] = .Missed
      return false
    }

    switch cellState {
    case .Filled:
      guard let ship = self.getShipWithCoordinate(coordinate: coordinate) else {
        return false
      }

      guard ship.proceedHit(coordinate: coordinate) else {
        return false
      }

      self.coordinatesDictionary[coordinate] = .Damaged

      if ship.shipState == .Destroyed {
        for coordinate in ship.coordinatesArray {
          self.coordinatesDictionary[coordinate] = .Destroyed
          self.neighborCoordinates(coordinate: coordinate, completion: {coordinate in
            if self.coordinatesDictionary[coordinate] == .Empty {
              self.coordinatesDictionary[coordinate] = .Missed
            }
          })
        }
      }

      return true
    case .Missed:
      return true
    case .Damaged, .Destroyed:
      return true
    case .Empty:
      self.coordinatesDictionary[coordinate] = .Missed
      return false
    }
  }

  func reloadField() {
    self.ships.removeAll()
    for horizontalIdentifier in 0..<10 {
      for verticalIdentifier in 0..<10 {
        self.coordinatesDictionary[Coordinate(vertical: verticalIdentifier, horizontal: horizontalIdentifier)] = .Empty
      }
    }
  }
}

// MARK: Collection View Data Source
extension BattleField: UICollectionViewDataSource {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 10 * 10
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.cellForItem(at: indexPath) else {
      return CollectionViewCell()
    }

    cell.backgroundColor = self.coordinatesDictionary[Coordinate(indexPath: indexPath)]?.color(battleMode: self.isBattleMode)
    return cell
  }
}

// MARK: Collection View Delegate
extension BattleField: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    guard self.isBattleMode else {
      return
    }

    let coordinate = Coordinate(indexPath: indexPath)
    let isHitted = self.proceedHit(coordinate: coordinate)

    collectionView.reloadItems(at: isHitted ? collectionView.indexPathsForVisibleItems : [indexPath])
    if !isHitted {
      Battle.switchPlayer()
    }

    if self.aliveShips.count == 0 {
      NotificationCenter.default.post(name: GameNotification.GameFinished, object: nil)
    }
  }
}
