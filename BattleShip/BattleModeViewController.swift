//
//  BattleModeViewController.swift
//  BattleShip
//
//  Created by Andrei Momot on 3/7/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import UIKit

enum GameState {
  case PlayerTurn
  case GameFinished

  enum Title: String {
    case playerTurn = ", it's your turn!"
    case finished = "Game finished!"
  }

  func title() -> String {
    switch self {
    case .PlayerTurn:
      return String("\(Battle.currentPlayer.name) \(Title.playerTurn.rawValue)")
    case .GameFinished:
      return Title.finished.rawValue
    }
  }
}

class BattleModeViewController: UIViewController {
  enum SegueIdentifier: String {
    case initial = "unwindToInitialViewController"
  }

  enum Alert: String {
    case controllerTitle = "Confirm"
    case finishedControllerTitle = "Congratulation!"
    case controllerMessage = "Are you shure to left the game?"
    case controllerFinishedMessage = " is the winner!"
    case okActionTitle = "OK"
    case yesActionTitle = "Yes"
    case noActionTitle = "No"
  }

  @IBOutlet weak var firstPlayerCollectionView: CollectionView!
  @IBOutlet weak var secondPlayerCollectionView: CollectionView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var backButton: UIButton!

    override func viewDidLoad() {
      super.viewDidLoad()
      self.observeForNotifications()

      Battle.attachCollectionViewToPlayerWithNumber(number: PlayerNumber.First,
                                                    collectionView: self.firstPlayerCollectionView)
      Battle.attachCollectionViewToPlayerWithNumber(number: PlayerNumber.Second,
                                                    collectionView: self.secondPlayerCollectionView)

      self.switchPlayer()
    }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.firstPlayerCollectionView.reloadItems(at: self.firstPlayerCollectionView.indexPathsForVisibleItems)
    self.secondPlayerCollectionView.reloadItems(at: self.secondPlayerCollectionView.indexPathsForVisibleItems)
  }

  deinit {
    NotificationCenter.default.removeObserver(self)
  }

  private func observeForNotifications() {
    NotificationCenter.default.addObserver(self, selector: #selector(finishGame),
                                           name: GameNotification.GameFinished, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(switchPlayer),
                                           name: GameNotification.PlayerSwitched, object: nil)
  }

  func switchPlayer() {
    self.titleLabel.text = GameState.PlayerTurn.title()

    if Battle.sharedBattle().currentPlayerNumber == .First {
      firstPlayerCollectionView.isUserInteractionEnabled = false
      secondPlayerCollectionView.isUserInteractionEnabled = true
      secondPlayerCollectionView.layer.borderWidth = 3.0
      secondPlayerCollectionView.layer.borderColor = UIColor.black.cgColor
      firstPlayerCollectionView.layer.borderColor = UIColor.clear.cgColor
    } else {
      firstPlayerCollectionView.isUserInteractionEnabled = true
      secondPlayerCollectionView.isUserInteractionEnabled = false
      firstPlayerCollectionView.layer.borderWidth = 3.0
      firstPlayerCollectionView.layer.borderColor = UIColor.black.cgColor
      secondPlayerCollectionView.layer.borderColor = UIColor.clear.cgColor
    }
  }

  @IBAction func onPressedBackButton(_ sender: Any) {
    let alertController = UIAlertController(title: Alert.controllerTitle.rawValue,
                                            message: Alert.controllerMessage.rawValue, preferredStyle: .alert)

    let yesAction = UIAlertAction(title: Alert.yesActionTitle.rawValue, style: .default) { _ in
      Battle.reloadGame()
      self.performSegue(withIdentifier: SegueIdentifier.initial.rawValue, sender: sender)
    }

    let cancelAction = UIAlertAction(title: Alert.noActionTitle.rawValue, style: .default, handler: nil)

    alertController.addAction(yesAction)
    alertController.addAction(cancelAction)

    self.present(alertController, animated: true, completion: nil)
  }

  func finishGame() {
    guard let defeatedPlayer = Battle.getWinnerName() else {
      return
    }

    let playerName = defeatedPlayer.name

    let gameFinished = UIAlertController(title: Alert.finishedControllerTitle.rawValue,
                                         message: "\(playerName) \(Alert.controllerFinishedMessage.rawValue)",
                                         preferredStyle: .alert)
    let okAction = UIAlertAction(title: Alert.okActionTitle.rawValue, style: .default) { _ in
      Battle.reloadGame()
      self.performSegue(withIdentifier: SegueIdentifier.initial.rawValue, sender: nil)
    }

    gameFinished.addAction(okAction)
    present(gameFinished, animated: true, completion: nil)

    self.titleLabel.text = GameState.GameFinished.title()
  }
}
